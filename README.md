# PythonOLD

Make the code faster. Make code writing faster. Make the code POZHILOY with Python OLympiaD. Dap da ya. Ta sho tam govorit...

# Usage

Every function is independent so you can just copy one and paste right to your code.

*  `True, False` can be written like `t, f`
*  `input()` can be written like `i()`
*  `input().split(str)` can be written like `i(str)`
*  `list(map(func, ar))` can be written like `m(func, ar)`
*  `[func(_) for _ in ar]` can be written like `g(func, ar)`
*  `lambda f, arg: func(arg)` can be written like `l(f, arg)` - Useful for sorted()
*  `sorted(ar, key=func, reverse=False)` can be written like `s(ar, k=func, r=f)`
*  `print(var, sep=str0, end=str1)` can be written like `p(var, s=str0, e=str1)`
*  `open('in.txt', 'r')` can be written like `o('in.txt')`
*  `file.close()` can be written like `c(file)`
*  `file.readlines()` can be written like `r(file)`
*  `file.write(str)` can be written like `w(file, str)`

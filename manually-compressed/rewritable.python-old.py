from sys import stdin, stdout;
t, f = True, False
i = lambda b=False: stdin.readline()[:-1].split(str(b)) if b else stdin.readline()[:-1]
m = lambda m, a: list(map(m, a))
g = lambda a, i: [a(_) for _ in i]
l = lambda f, a: f(a)
p = lambda *args, **kwargs: stdout.write((kwargs['s'] if 's' in kwargs else ' ').join([str(_) for _ in args]) + (kwargs['e'] if 'e' in kwargs else '\n'))
s = lambda *args, **kwargs: sorted(args[0], key=kwargs['k'] if 'k' in kwargs else None, reverse=kwargs['r'] if 'r' in kwargs else False)
o = lambda *args: open(args[0], 'r') if len(args) == 1 else open(args[0], args[1])
c = lambda f: f.close()
r = lambda f: f.readlines()
w = lambda f, s: f.write(str(s))

# Example
p(*s(m(int, i(' ')), r=t), s=' 89 ')
